//
//  PhotoCollectionViewCell.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 8/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class PhotoCollectionViewCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var image: UIImageView!
    
}
