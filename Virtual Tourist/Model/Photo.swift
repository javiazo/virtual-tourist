//
//  Photo.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import CoreData


final class Photo: NSManagedObject {

    convenience init(url: String, image: NSData?,  context : NSManagedObjectContext) {
        
        if let ent = NSEntityDescription.entityForName("Photo",
                                                       inManagedObjectContext: context){
            self.init(entity: ent, insertIntoManagedObjectContext: context)
            self.url = url
            self.image = image
        } else {
            fatalError("Unable to find Entity name!")
        }
        
    }

}
