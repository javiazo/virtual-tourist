//
//  Pin.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import CoreData
import MapKit


final class Pin: NSManagedObject, MKAnnotation {
    
    private var _coords: CLLocationCoordinate2D?
    var coordinate: CLLocationCoordinate2D {
        
        set {
            willChangeValueForKey("coordinate")
            _coords = newValue
            if let coord = _coords {
                lat = coord.latitude
                lon = coord.longitude
            }
            didChangeValueForKey("coordinate")
        }
        
        get {
            if _coords == nil {
                _coords = CLLocationCoordinate2DMake(lat as CLLocationDegrees, lon as CLLocationDegrees)
            }
            
            return _coords!
        }
    }

    convenience init(lat: Double, lon: Double,  context : NSManagedObjectContext) {
        
        if let ent = NSEntityDescription.entityForName("Pin",
                                                       inManagedObjectContext: context){
            self.init(entity: ent, insertIntoManagedObjectContext: context)
            self.lat = lat
            self.lon = lon
        } else {
            fatalError("Unable to find Entity name!")
        }
        
    }

}
