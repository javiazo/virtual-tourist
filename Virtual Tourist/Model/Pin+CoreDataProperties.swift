//
//  Pin+CoreDataProperties.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Pin {

    @NSManaged var lat: NSNumber
    @NSManaged var lon: NSNumber
    @NSManaged var photos: NSSet?
    @NSManaged var pages: NSNumber?

}
