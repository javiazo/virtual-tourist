//
//  PhotoAlbumViewController.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit
import MapKit
import CoreData

final class PhotoAlbumViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var newCollectionButton: UIButton!
    
    // Selected pin
    var selectedPin: Pin!
    
    // Deleted index paths
    private var dip: [NSIndexPath] = []
    
    // CoreDataStack
    private let stack = CoreDataStack.sharedInstance
    
    // Fetched Results Controller
    var fetchedResultsController : NSFetchedResultsController?{
        didSet{
            // Whenever the frc changes, we execute the search and
            // reload the table
            fetchedResultsController?.delegate = self
            executeSearch()
            checkPhotosStatus()
        }
    }

    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure view
        configureView()
        
        // Create a fetchrequest
        let fr = NSFetchRequest(entityName: "Photo")
        fr.sortDescriptors = [NSSortDescriptor(key: "url", ascending: true)]
        
        let pred = NSPredicate(format: "pin = %@", argumentArray: [selectedPin])
        fr.predicate = pred
        
        // Create the FetchedResultsController
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fr,
                                                              managedObjectContext: stack.context, sectionNameKeyPath: nil, cacheName: nil)
    }

    // MARK: View configuration
    private func configureView() {
        // Title
        title = NSLocalizedString("photo-album", comment: "")
        
        // New collection button
        newCollectionButton.setTitle(NSLocalizedString("new-collection", comment: ""), forState: .Normal)
        newCollectionButton.enabled = false
        
        // Add pin to map and zoom
        // Annotation
        mapView.addAnnotation(selectedPin)
        
        // Zoom
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegion(center: selectedPin.coordinate, span: span)
        mapView.setRegion(region, animated: false)
        
        // Round map corners
        mapView.layer.cornerRadius = 15.0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // CollectionView Layout
        let width = (CGRectGetWidth(collectionView.frame) / 3) - 5
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: width, height: width)
        }
    }
    
    // MARK: Check for collection updates
    private func checkPhotosStatus() {
        guard let fc = fetchedResultsController else { return }
        guard let firstSection = fc.sections?.first else { return }
        guard let photos = firstSection.objects as? [Photo] else { return }
        
        if photos.count == 0 {
            retrievePhotos()
        }
        
        // Enable new collection button
        newCollectionButton.enabled = true
    }
    
    // MARK: Retrieve photos from API
    private func retrievePhotos() {
        // Get photos from Flickr
        FlickrAPI.sharedInstance.photosForPin(selectedPin) { (result) in
            switch result {
            case .Success(let photosResult):
                // Save photos (w/ id) in DB
                if photosResult.urls.count == 0 { self.noPhotosAlert() }
                self.stack.performBackgroundBatchOperation({ (_) in
                    self.selectedPin.pages = photosResult.pages
                    for photoUrl in photosResult.urls {
                        let photo = Photo(url: photoUrl, image: nil, context: self.selectedPin.managedObjectContext!)
                        photo.pin = self.selectedPin
                    }
                })
                break
            case .Failure(let errStr):
                print(errStr)
                break
            }
        }
    }
    
    // MARK: No photos alert
    private func noPhotosAlert() {
        let alert = UIAlertController(title: NSLocalizedString("no-photos-title", comment: ""), message: NSLocalizedString("no-photos-msg", comment: ""), preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .Default, handler: nil)
        alert.addAction(okAction)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: New collection
    @IBAction func newCollection(sender: AnyObject) {
        // Remove all the photos from the collection
        guard let photosToDelete = fetchedResultsController?.fetchedObjects else { return }
        
        for photo in photosToDelete {
            let photo = photo as! Photo
            fetchedResultsController?.managedObjectContext.deleteObject(photo)
        }
        
        // Request new photos
        retrievePhotos()
    }
    
    
}

// MARK:  - Fetches
extension PhotoAlbumViewController {
    
    func executeSearch(){
        if let fc = fetchedResultsController{
            do{
                try fc.performFetch()
            } catch let e as NSError{
                print("Error while trying to perform a search: \n\(e)\n\(fetchedResultsController)")
            }
        }
    }
}

// MARK:  - Delegate
extension PhotoAlbumViewController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        if dip.count > 0 {
            collectionView.performBatchUpdates({ 
                self.collectionView.deleteItemsAtIndexPaths(self.dip)
                self.dip = []
                }, completion: nil)
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                self.collectionView.reloadData()
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController,
                    didChangeObject anObject: AnyObject,
                                    atIndexPath indexPath: NSIndexPath?,
                                                forChangeType type: NSFetchedResultsChangeType,
                                                              newIndexPath: NSIndexPath?) {
        
        guard let indexPath = indexPath else { return }
        
        switch(type) {
        case .Delete:
            dip.append(indexPath)
            break
        case .Update:
            dispatch_async(dispatch_get_main_queue()) {
                self.collectionView.reloadItemsAtIndexPaths([indexPath])
            }
            break
        default:
            break
        }
        
        // Save on disk
        stack.save()
        
    }
    
}

// MARK: UICollectionView DataSource
extension PhotoAlbumViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photo", forIndexPath: indexPath) as! PhotoCollectionViewCell
        
        // Check if the photo has an image
        guard let photo = fetchedResultsController?.objectAtIndexPath(indexPath) as? Photo else {
            return cell
        }
        
        // Avoid have the previous photo while loading the new one
        cell.image.image = nil
        
        if let imageData = photo.image {
            cell.image.image = UIImage(data: imageData)
        } else {
            FlickrAPI.sharedInstance.imageWithUrl(photo.url, completion: { (result) in
                switch result {
                case .Success(let data):
                    dispatch_async(dispatch_get_main_queue()) {
                        photo.image = data
                    }
                    break
                case .Failure(let errStr):
                    print(errStr)
                    break
                }
            })
        }
        
        return cell
    }
    
}

// MARK: UICollectionView Delegate
extension PhotoAlbumViewController: UICollectionViewDelegate {

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        guard let fc = fetchedResultsController else { return }
        guard let photoToDelete = fc.objectAtIndexPath(indexPath) as? Photo else { return }
        
        // Delete photo
        fc.managedObjectContext.deleteObject(photoToDelete)
    }

}
