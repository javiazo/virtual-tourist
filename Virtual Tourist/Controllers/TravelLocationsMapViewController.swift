//
//  TravelLocationsMapViewController.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit
import MapKit
import CoreData

final class TravelLocationsMapViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // Annotation identifier
    private let annotationIdentifier = "pin"
    
    // CoreDataStack
    private let stack = CoreDataStack.sharedInstance
    
    // Fetched Results Controller
    var fetchedResultsController : NSFetchedResultsController?{
        didSet{
            // Whenever the frc changes, we execute the search and
            // reload the table
            fetchedResultsController?.delegate = self
            executeSearch()
            reloadMapAnnotations()
        }
    }

    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure view
        configureView()
        
        // Add gesture
        mapView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:))))
        
        // Create a fetchrequest
        let fr = NSFetchRequest(entityName: "Pin")
        fr.sortDescriptors = [NSSortDescriptor(key: "lat", ascending: true)]
        
        // Create the FetchedResultsController
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fr,
                                                              managedObjectContext: stack.context, sectionNameKeyPath: nil, cacheName: nil)
    }
    
    // MARK: View configuration
    private func configureView() {
        // Title
        title = NSLocalizedString("travel-locations", comment: "")
    }
    
    // MARK: Long gesture tap
    func longTap(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .Began:
            // Get coordinates
            let coordinates = mapView.convertPoint(sender.locationInView(mapView), toCoordinateFromView: mapView)
            
            // Add pin
            addPinWithCoordinates(coordinates)
        default: break
        }
    }
    
    // MARK: Add pin
    private func addPinWithCoordinates(coordinates: CLLocationCoordinate2D) {
        // Create pin
        let _ = Pin(lat: coordinates.latitude, lon: coordinates.longitude, context: stack.context)
        
        // Save pin
        stack.save()
    }
    
    // MARK: Reload pins
    private func reloadMapAnnotations() {
        guard let fc = fetchedResultsController else { return }
        guard let firstSection = fc.sections?.first else { return }
        guard let pins = firstSection.objects as? [Pin] else { return }
        mapView.addAnnotations(pins)
    }
    
    // MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let segueId = segue.identifier else { return }
        
        switch segueId {
        case "album":
            let destVC = segue.destinationViewController as! PhotoAlbumViewController
            let pin = sender as! Pin
            destVC.selectedPin = pin
            break
        default: break
        }
    }

}

// MARK: Map View Delegate
extension TravelLocationsMapViewController: MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(annotationIdentifier) as? MKPinAnnotationView
        
        // Configure pin
        if let pin = pin {
            pin.annotation = annotation
        } else {
            pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            pin?.canShowCallout = false
        }
        
        // Animate pin
        pin?.animatesDrop = true
        
        return pin
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        // Deselect pin
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        // Navigate to detail view
        guard let pin = view.annotation as? Pin else {
            print("Annotation is not a Pin")
            return
        }
        performSegueWithIdentifier("album", sender: pin)
    }
}

// MARK:  - Fetches
extension TravelLocationsMapViewController {
    
    func executeSearch(){
        if let fc = fetchedResultsController{
            do{
                try fc.performFetch()
            }catch let e as NSError{
                print("Error while trying to perform a search: \n\(e)\n\(fetchedResultsController)")
            }
        }
    }
}

// MARK:  - Delegate
extension TravelLocationsMapViewController: NSFetchedResultsControllerDelegate {

    func controller(controller: NSFetchedResultsController,
                    didChangeObject anObject: AnyObject,
                                    atIndexPath indexPath: NSIndexPath?,
                                                forChangeType type: NSFetchedResultsChangeType,
                                                              newIndexPath: NSIndexPath?) {
        
        
        // Reload annotations
        reloadMapAnnotations()
    }

}
