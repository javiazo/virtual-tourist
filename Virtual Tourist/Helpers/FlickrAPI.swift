//
//  FlickrAPI.swift
//  Virtual Tourist
//
//  Created by Javier Azorín Fernández on 7/8/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import UIKit

// MARK: Result enum (w/ generics)
enum Result<T> {
    case Success(T)
    case Failure(String)
}

// MARK: Photos result
struct PhotosResult {
    var urls: [String]
    var pages: Int
}

final class FlickrAPI {

    // FlickrAPI Singleton
    static let sharedInstance = FlickrAPI()
    
    // Constants
    private let Url = "https://api.flickr.com/services/rest"
    private let ApiKey = "150b5273e8e88b2e5eb2cebcf8ad4dfe"
    private let Method = "flickr.photos.search"
    private let Format = "json"
    private let NoJSONCallback = "1"
    private let Media = "photos"
    private let PerPage = "30"
    
    // MARK: API calls
    // MARK: Photos
    /**
     Gets photos within a location
     
     - parameter coordinate: Latitude and longitude
     - parameter completion: Completion block containing an array of photos if everything went well, or an error.
     */
    func photosForPin(pin: Pin, completion: (result: Result<PhotosResult>) ->  Void) {
        
        // Page
        var page: Int?
        if let pages = pin.pages {
            page = randRange(1, upper: Int(pages))
        }
        
        let methodArguments = [
            "method": Method,
            "api_key": ApiKey,
            "format": Format,
            "nojsoncallback": NoJSONCallback,
            "per_page": PerPage,
            "lat": pin.lat,
            "lon": pin.lon,
            "page": page ?? 1
        ]
        
        let urlString = Url + escapedParameters(methodArguments)
        
        guard let url = NSURL(string: urlString) else {
            completion(result: .Failure("Error with the URL!"))
            return
        }
        
        let request = NSURLRequest(URL: url)
        
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            guard error == nil else {
                completion(result: .Failure(error?.localizedDescription ?? "Error making the request!"))
                return
            }
            
            guard let data = data else {
                completion(result: .Failure("Error getting the data!"))
                return
            }
            
            do {
                guard let result = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? [String : AnyObject] else {
                    completion(result: .Failure("Error getting the JSON!"))
                    return
                }
                
                guard let photosDic = result["photos"]?["photo"] as? [[String : AnyObject]] else {
                    completion(result: .Failure("Error getting photos from JSON!"))
                    return
                }
                
                let urls = photosDic.flatMap({ item in
                    self.urlFromDictionary(item)
                })
                
                let pages = result["photos"]?["pages"] as? Int ?? 1
                
                let photosResult = PhotosResult(urls: urls, pages: pages)

                completion(result: .Success(photosResult))
            } catch {
                return
            }
            }.resume()
        
    }
    
    /// Random number in range
    private func randRange (lower: Int , upper: Int) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
    }
    
    // MARK: - Escaped parameters
    /**
     Creates a String with all the parameters for a URL
     
     - parameter dic: Dictionary of key:value parameters
     */
    private func escapedParameters(dic: [String : AnyObject]) -> String {
        guard dic.count > 0 else { return "" }
        
        var str = "?"
        
        for (index, item) in dic.enumerate() {
            str = str + "\(item.0)=\(item.1)"
            if index < dic.count - 1 {
                str = str + "&"
            }
        }
        
        return str
    }
    
    // MARK: - Image (NSData) from dictionary
    /**
     Returns an image from an URL created with a dictionary information
     
     - parameter dic: Dictionary of key:value parameters
     - returns: NSData image from the URL
     */
    private func urlFromDictionary(dic: [String: AnyObject]) -> String? {
        // Build URL from dictionary
        guard let farm = dic["farm"] as? Int,
                server = dic["server"] as? String,
                id = dic["id"] as? String,
            secret = dic["secret"] as? String else { return nil }
        
        return "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_q.jpg"
    }
    
    /**
     Get the image from the URL
     
     - parameter imageUrl: Image url
     - parameter completion: Call result (Success or Failure)
     */
    func imageWithUrl(imageUrl: String, completion: (result: Result<NSData>) -> Void) {
        // Image from URL
        guard let url = NSURL(string: imageUrl) else {
            completion(result: .Failure("Error with the image url!"))
            return
        }
        
        // Request
        let request = NSURLRequest(URL: url)
        
        // Call
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            // Get the data
            guard let data = data else {
                completion(result: .Failure("Error reciving the data!"))
                return
            }
            
            // Return the image
            completion(result: .Success(data))
            }.resume()

    }

}
